<html>
	<head>
		<title>Hoaem</title>

		<link href='http://fonts.googleapis.com/css?family=Lato:100' rel='stylesheet' type='text/css'>

		<style>
			body {
				margin: 0;
				padding: 0;
				width: 100%;
				height: 100%;
				color: #f2f199;
				display: table;
				font-weight: 100;
				font-family: 'Lato';
				background: #634e42;
			}

			.container {
				text-align: center;
				display: table-cell;
				vertical-align: middle;
			}

			.content {
				text-align: center;
				display: inline-block;
			}

			.title {
				font-size: 96px;
				margin-bottom: 40px;
			}

			.quote {
				font-size: 24px;
			}
		</style>
	</head>
	<body>
		<div class="container">
			<div class="content">
				<div class="title">Protibi</div>
				<div class="quote">Simplify for world!</div>
			</div>
		</div>
	</body>
</html>
