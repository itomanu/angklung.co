<!DOCTYPE html>
<html>
<head>
	<title>Thank You!</title>
</head>
<body>
	<table align="center" border="0" cellpadding="0" cellspacing="0" style="max-width:600px; min-width; 350px; font-family:arial, sans-serif; font-size: 13px;" width="100%">
		<tr>
			<td style="background:#f4f1a0;color:#634e42;padding: 10px 30px;" colspan="2">
				<p style="padding: 0; text-align: center;">
					<img align="none" width="232" height="93" src="http://angklung.co/images/logo.png" style="width: 232px; height: 93px;">
				</p>
			</td>
		</tr>
		<tr>
			<td style="background:#f4f1a0;color:#634e42;padding: 10px 30px;" colspan="2">
				<p style="font-size:14px; line-height: 27px;">
					<strong>Hi <?= $name ?>,</strong>
				</p>
				<p style="font-size:13px; line-height: 21px;">
			    	Thanks for sending us an email.<br>
			    	This is an auto reply email, we will contact you soon!<br/><br/>

			    	If you have any feedback or anything to say, please let us know and together we make a better world.<br/><br/>

			    	Victor Manu, Founder
			    </p>
			</td>
		</tr>
		<tr style="background:#222;color:#ddd;margin-top:5px;">
			<td style="padding: 10px 30px;">
				<p style="font-size:13px; line-height: 21px;text-align:right;color:#ddd">
					<strong style="color:#f4f1a0">Angklung Studio</strong><br/>
			    	Jl. Babakan Jeruk 3C No. 4<br/>
			    	40163 Sukajadi, Bandung<br/>
			    	Jawa Barat - Indonesia<br/>
			    	Tlp. 022-87790331
			    </p>
			</td>
		</tr>
	</table>
</body>
</html>