<!DOCTYPE HTML>
<html>
	<head>
		<title>Angklung Studio</title>
		<meta name="description" content="Page description. No longer than 155 characters." />

		<!-- Schema.org markup for Google+ -->
		<meta itemprop="name" content="Angklung Studio">
		<meta itemprop="description" content="Together we make symphony">
		<meta itemprop="image" content="<?=base_url()?>images/angklung_wall.jpg">

		<!-- Twitter Card data -->
		<meta name="twitter:card" content="summary_large_image">
		<meta name="twitter:site" content="@angklungstudio">
		<meta name="twitter:title" content="Angklung Studio">
		<meta name="twitter:description" content="Together we make symphony">
		<meta name="twitter:creator" content="@angklungstudio">
		<!-- Twitter summary card with large image must be at least 280x150px -->
		<meta name="twitter:image:src" content="<?=base_url()?>images/angklung_wall.jpg">

		<!-- Open Graph data -->
		<meta property="og:title" content="Angklung Studio" />
		<meta property="og:type" content="website" />
		<meta property="og:url" content="http://www.angklung.co/" />
		<meta property="og:image" content="<?=base_url()?>images/angklung_wall.jpg" />
		<meta property="og:description" content="Together we make symphony" />
		<meta property="og:site_name" content="Angklung Studio" />

		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="shortcut icon" type="image/x-icon" href="<?= base_url()?>images/fave-icon.png" />

		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
		<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
		</script>

   		<link href="<?= base_url()?>css/style.css" rel="stylesheet" type="text/css" media="all" />
   		<link href="<?= base_url()?>css/screen.css" media="screen, projection" rel="stylesheet" type="text/css" />
		<script src="<?= base_url()?>js/modernizr.custom.28468.js"></script>
		<link rel="stylesheet" type="text/css" href="<?= base_url()?>css/simptip-mini.css" media="screen,projection" />

		<!-- start Mixpanel --><script type="text/javascript">(function(f,b){if(!b.__SV){var a,e,i,g;window.mixpanel=b;b._i=[];b.init=function(a,e,d){function f(b,h){var a=h.split(".");2==a.length&&(b=b[a[0]],h=a[1]);b[h]=function(){b.push([h].concat(Array.prototype.slice.call(arguments,0)))}}var c=b;"undefined"!==typeof d?c=b[d]=[]:d="mixpanel";c.people=c.people||[];c.toString=function(b){var a="mixpanel";"mixpanel"!==d&&(a+="."+d);b||(a+=" (stub)");return a};c.people.toString=function(){return c.toString(1)+".people (stub)"};i="disable track track_pageview track_links track_forms register register_once alias unregister identify name_tag set_config people.set people.set_once people.increment people.append people.union people.track_charge people.clear_charges people.delete_user".split(" ");
for(g=0;g<i.length;g++)f(c,i[g]);b._i.push([a,e,d])};b.__SV=1.2;a=f.createElement("script");a.type="text/javascript";a.async=!0;a.src="undefined"!==typeof MIXPANEL_CUSTOM_LIB_URL?MIXPANEL_CUSTOM_LIB_URL:"//cdn.mxpnl.com/libs/mixpanel-2-latest.min.js";e=f.getElementsByTagName("script")[0];e.parentNode.insertBefore(a,e)}})(document,window.mixpanel||[]);
mixpanel.init("1fbb8b8be235e21c16aa11bf709b189b");</script><!-- end Mixpanel -->

		<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-48993862-3', 'auto');
  ga('send', 'pageview');

</script>
	</head>
	<body>
		<script type="text/javascript">
			mixpanel.track("Visit Home Page");
		</script>
		<!---start-wrap--- -->
			<!---start-header--- -->
			<div class="header" id="home">
				<div class="wrap">
				<div class="top-header">
					<div class="logo">
						<a href="#"><span> </span></a>
					</div>
					<div class="top-nav">
						<ul>
							<li class="active"><a id="link-home" href="#home" class="scroll">Home</a></li>
							<li><a id="link-services" href="#services" class="scroll">Services</a></li>
							<li><a id="link-portfolio" href="#portfolio" class="scroll">Portfolio</a></li>
							<!-- <li><a id="link-team" href="#team" class="scroll">Team</a></li> -->
							<li><a id="link-contact" href="#contact" class="scroll">Contact</a></li>
							<div class="clear"> </div>
						</ul>
					</div>
					<div class="clear"> </div>
				</div>
			<!---End-header--- -->
			<!----start-content-slider---->
			<div id="da-slider" class="da-slider">
					  <div id="intro" class="da-slide">
					    <div class="da-title">
					      <h3>Welcome to<br />Angklung Studio!</h3>
					    </div>
					    <div class="da-img">
					      <img src="<?= base_url()?>/images/slider-pic1.png" alt="Drupal Powered" title="Presenting the Drupalien" />
					    </div>
					  </div>
					  <div id="user-experience" class="da-slide">
					   <div class="da-title">
					   <h3>Need a Web Site?<br/>We ready for You!</h3>
					    </div>
					     <div class="da-img">
					      <img src="<?= base_url()?>/images/slider-pic2.png" alt="Drupal Powered" title="Presenting the Drupalien" />
					    </div>
					  </div>
					  <div id="design" class="da-slide">
					   <div class="da-title">
					    <h3>Mobile Apps?<br />Don't mention it!</h3>
					    </div>
					     <div class="da-img">
					      <img src="<?= base_url()?>/images/slider-pic3.png" alt="Drupal Powered" title="Presenting the Drupalien" />
					    </div>
					  </div>
					  <div>
					  </div>
					</div>
					<script src="<?= base_url()?>js/jquery.cslider.js"></script>
					<script>
					//jQuery(document).ready(function() {
					$('#da-slider').cslider({
					  autoplay : true,
					  interval: 3000,
					  bgincrement : 10
					});
					//});
					</script>
		<!----start-content-slider---->
			</div>
		</div>
		<!---End-header--- -->
		<!---start-content---->
		<div class="content">
			<div class="wrap">
			<!---start-top-grids---->
				<div class="top-service-grids" id="services">
					<div class="top-service-grid">
						<a href="#"><img src="<?= base_url()?>images/top-grid-pic1.png" alt="" /></a>
						<a href="#"><span>Web or Mobile Apps</span></a>
						<p>We are concentrate on making mobile apps (Android or iOS) as well as the website.</p>
					</div>
					<div class="top-service-grid">
						<a href="#"><img src="<?= base_url()?>images/top-grid-pic2.png" alt="" /></a>
						<a href="#"><span>Make It Happen</span></a>
						<p>Have an idea in your head, and want to make it happen, contact us right know and let's start working with us.</p>
					</div>
					<div class="top-service-grid">
						<a href="#"><img src="<?= base_url()?>images/top-grid-pic4.png" alt="" /></a>
						<a href="#"><span>Team Work</span></a>
						<p>Our team is ready for you, contact us know.</p>
					</div>
					<div class="clear"> </div>
				</div>
			<!---End-top-grids---->
			</div>
			<!---start-recent-works--- -->
			<div class="recent-works" id="portfolio">
				<div class="wrap">
				<div class="recent-works-head">
					<h3>Our works</h3>
					<p>Here is some of our works.</p>
				</div>
				<!---start-mfp ---->	
			<div id="chordrive-dialog" class="mfp-hide sm-dialog1">
				<div class="pop_up">
					<img src="<?= base_url()?>images/p1.jpg" alt="Image 2" style="top: 0px;">
					<h2>Chordrive</h2>
					<p class="para">Service that provide you lyrics and chords repository, that you can get free.<br/>
					Available in mobile application as well as website.<br/>
					<a id="cd-web" href="http://www.chordrive.com/">Visit website</a></p>
					<p class="">Release soon...</p>
				</div>
			</div>
			<div id="warna-dialog" class="mfp-hide sm-dialog1">
				<div class="pop_up">
					<img src="<?= base_url()?>images/p2.jpg" alt="Image 2" style="top: 0px;">
					<h2>Warna Studio</h2>
					<p class="para">Social network and business portal for Makeup Artists, wedding vendors, and kind.<br/>
					Available in mobile application as well as website.<br/>
					<a id="ws-web" href="http://warnastudio.com/">Visit website</a></p>
					<p class="">Release soon...</p>
				</div>
			</div>
			<div id="hibandung-dialog" class="mfp-hide sm-dialog1">
				<div class="pop_up">
					<img src="<?= base_url()?>images/p3.jpg" alt="Image 2" style="top: 0px;">
					<h2>Hi Bandung</h2>
					<p class="para">Application which provides the most complete information about point of interest in Bandung.<br/>
					This application can be used by tourists to travel to Bandung.<br/>
					Available in mobile application (<a id="hb-and" href="https://play.google.com/store/apps/details?id=id.telkom.hbandung&hl=en">Android</a> & <a id="hb-ios" href="https://itunes.apple.com/app/id845102595">iOS</a>).</p>
					<p class="">Working with Telkom RDC.</p>
				</div>
			</div>
			<div id="haratishare-dialog" class="mfp-hide sm-dialog1">
				<div class="pop_up">
					<img src="<?= base_url()?>images/p4.jpg" alt="Image 2" style="top: 0px;">
					<h2>Haratishare</h2>
					<p class="para">Discover more of the news you care about all in one app. Enjoy sharing news and get more point to reedem any best stuff.<br/>
					Available in <a id="hs-and" href="https://play.google.com/store/apps/details?id=com.haratishare&hl=en">Android</a> application as well as <a id="hs-web" href="http://www.haratishare.com/">website</a>.</p>
					<p class="">Working with Haratishare.</p>
				</div>
			</div>
			<div id="hiindonesia-dialog" class="mfp-hide sm-dialog1">
				<div class="pop_up">
					<img src="<?= base_url()?>images/p5.jpg" alt="Image 2" style="top: 0px;">
					<h2>Hi Indonesia</h2>
					<p class="para">Help You Travel More, online Hotel & Flight reservations.</p>
					<p class="">Working with Telkom RDC.</p>
				</div>
			</div>
			<div id="small-dialog1" class="mfp-hide">
				<div class="pop_up">
					<h2>Yours text place Here</h2>
					<p class="para">Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet.</p>
				</div>
			</div>
			<!---end-mfp ---->
				<div class="gallery">
					<div class="clear"> </div>
					<div class="container">
					<ul id="filters" class="clearfix">
						<li id="portfolio-all"><span class="filter active" data-filter="app web mobile design">All</span></li>
						<li id="portfolio-web"><span class="filter" data-filter="app web">Web</span></li>
						<li id="portfolio-mobile"><span class="filter" data-filter="app mobile">Mobile</span></li>
						<li id="portfolio-design"><span class="filter" data-filter="design">Design</span></li>
					</ul>
						<div id="portfoliolist" style="     ">
						<div id="portfolio-chordrive" class="portfolio app mix_all" data-cat="app" style=" ">
							<div class="portfolio-wrapper">				
								<a class="popup-with-zoom-anim" href="#chordrive-dialog">
									<img src="<?= base_url()?>images/p1.jpg" alt="Image 2" style="top: 0px;">
								</a>
								<div class="label" style="bottom: -40px;">
									<div class="label-text">
										<a class="text-title">Chordrive</a>
										<span class="text-category">APP</span>
									</div>
									<div class="label-bg"></div>
								</div>
							</div>
						</div>				
						<div id="portfolio-warna" class="portfolio app mix_all" data-cat="app" style="  display: inline-block; opacity: 1;">
							<div class="portfolio-wrapper">			
								<a class="popup-with-zoom-anim" href="#warna-dialog">
									<img src="<?= base_url()?>images/p2.jpg" alt="Image 2" style="top: 0px;">
								</a>
								<div class="label" style="bottom: -40px;">
									<div class="label-text">
										<a class="text-title">Warna Studio</a>
										<span class="text-category">APP</span>
									</div>
									<div class="label-bg"></div>
								</div>
							</div>
						</div>		
						<div id="portfolio-hibandung" class="portfolio app mix_all" data-cat="app" style="  display: inline-block; opacity: 1;">
							<div class="portfolio-wrapper">						
								<a class="popup-with-zoom-anim" href="#hibandung-dialog">
									<img src="<?= base_url()?>images/p3.jpg" alt="Image 2" style="top: 0px;">
								</a>
								<div class="label" style="bottom: -40px;">
									<div class="label-text">
										<a class="text-title">Hi Bandung with Telkom RDC</a>
										<span class="text-category">APP</span>
									</div>
									<div class="label-bg"></div>
								</div>
							</div>
						</div>				
						<div id="portfolio-haratishare" class="portfolio app mix_all" data-cat="app" style="  display: inline-block; opacity: 1;">
							<div class="portfolio-wrapper">			
								<a class="popup-with-zoom-anim" href="#haratishare-dialog">
									<img src="<?= base_url()?>images/p4.jpg" alt="Image 2" style="top: 0px;">
								</a>
								<div class="label" style="bottom: -40px;">
									<div class="label-text">
										<a class="text-title">Haratishare</a>
										<span class="text-category">APP</span>
									</div>
									<div class="label-bg"></div>
								</div>
							</div>
						</div>	
									
						<div id="portfolio-hiindo" class="portfolio design mix_all" data-cat="design" style="  display: inline-block; opacity: 1;">
							<div class="portfolio-wrapper">
								<a class="popup-with-zoom-anim" href="#hiindonesia-dialog">
									<img src="<?= base_url()?>images/p5.jpg" alt="Image 2" style="top: 0px;">
								</a>
								<div class="label" style="bottom: -40px;">
									<div class="label-text">
										<a class="text-title">Hi Indonesia with Telkom RDC</a>
										<span class="text-category">Web Design</span>
									</div>
									<div class="label-bg"></div>
								</div>
							</div>
						</div>			
					</div>
				</div><!-- container -->

				<div class="clear"> </div>
				<script type="text/javascript" src="<?= base_url()?>js/fliplightbox.min.js"></script>
				<script type="text/javascript">$('body').flipLightBox()</script>
				<!---start-gallery-script--- -->
					<script type="text/javascript" src="<?= base_url()?>js/jquery.easing.min.js"></script>	
					<script type="text/javascript" src="<?= base_url()?>js/jquery.mixitup.min.js"></script>
					<script type="text/javascript">
					$(function () {
						
						var filterList = {
						
							init: function () {
							
								// MixItUp plugin
								// http://mixitup.io
								$('#portfoliolist').mixitup({
									targetSelector: '.portfolio',
									filterSelector: '.filter',
									effects: ['fade'],
									easing: 'snap',
									// call the hover effect
									onMixEnd: filterList.hoverEffect()
								});				
							
							},
							
							hoverEffect: function () {
							
								// Simple parallax effect
								$('#portfoliolist .portfolio').hover(
									function () {
										$(this).find('.label').stop().animate({bottom: 0}, 200, 'easeOutQuad');
										$(this).find('img').stop().animate({top: -30}, 500, 'easeOutQuad');				
									},
									function () {
										$(this).find('.label').stop().animate({bottom: -40}, 200, 'easeInQuad');
										$(this).find('img').stop().animate({top: 0}, 300, 'easeOutQuad');								
									}		
								);				
							}
						};
						// Run the show!
						filterList.init();
					});	
					</script>
					<!-- Add fancyBox main JS and CSS files -->
					<script src="<?= base_url()?>js/jquery.magnific-popup.js" type="text/javascript"></script>
					<link href="<?= base_url()?>css/magnific-popup.css" rel="stylesheet" type="text/css">
							<script>
								$(document).ready(function() {
									$('.popup-with-zoom-anim').magnificPopup({
										type: 'inline',
										fixedContentPos: false,
										fixedBgPos: true,
										overflowY: 'auto',
										closeBtnInside: true,
										preloader: false,
										midClick: true,
										removalDelay: 300,
										mainClass: 'my-mfp-zoom-in'
								});
							});
							</script>
				<!---//End-gallery-script--- -->
				<div class="clear"> </div>
				</div>
			</div>
			</div>
			<!---End-recent-works--- -->
			
			<!---start-meet-our-team---->
			<!--
			<div class="team" id="team">
				<div class="wrap">
					<div class="team-head">
						<h3>Here we are</h3>
						<p>We are not the biggest, but with the same vision to make things new and better together as Angklung.</p>
					</div>
					<script type="text/javascript">
					</script>
					<div class="team-members">
						<div class="team-member">
							<span><a href="#team"><img src="<?= base_url()?>images/victor-manu.jpg"></a></span>
							<h4><a href="#team">Victor Manu</a></h4>
						   <label>Founder</label>
							<p>Simple man, experience in Web, Mobile & Game Development.</p>
							<ul>
								<li>
									<li><a class="facebook simptip-position-bottom simptip-movable" data-tooltip="Facebook" href="https://www.facebook.com/itomanu" target="_blank"> </a></li>
									<li><a class="mail simptip-position-bottom simptip-movable" data-tooltip="Mail" href="mailto:itomanu@angklung.co" target="_blank"> </a></li>
									<li><a class="linkedin simptip-position-bottom simptip-movable" data-tooltip="LinkedIn" href="https://id.linkedin.com/in/itomanu" target="_blank"> </a></li>
									<div class="clear"> </div>
								</li>
							</ul>
						</div>
						<div class="team-member">
							<span><a href="#team"><img src="<?= base_url()?>images/boy-gorby.jpg"></a></span>
							<h4><a href="#team">Boy Gorby</a></h4>
						   <label>Founder</label>
							<p>Years of experience in web & mobile development.</p>
							<ul>
								<li>
									<li><a class="facebook simptip-position-bottom simptip-movable" data-tooltip="Facebook" href="https://www.facebook.com/bigyst" target="_blank"> </a></li>
									<li><a class="mail simptip-position-bottom simptip-movable" data-tooltip="Mail" href="mailto:gorby@angklung.co" target="_blank"> </a></li>
									<li><a class="linkedin simptip-position-bottom simptip-movable" data-tooltip="LinkedIn" href="https://id.linkedin.com/in/boygorby" target="_blank"> </a></li>
									<div class="clear"> </div>
								</li>
							</ul>
						</div>
						<div class="team-member">
							<span><a href="#team"><img src="<?= base_url()?>images/tommy-sugianto.jpg"></a></span>
							<h4><a href="#team">Tommy Sugianto</a></h4>
						   <label>Founder</label>
							<p>Years of experience as Android Developer.</p>
							<ul>
								<li>
									<li><a class="facebook simptip-position-bottom simptip-movable" data-tooltip="Facebook" href="https://www.facebook.com/kurniawan.tommy" target="_blank"> </a></li>
									<li><a class="mail simptip-position-bottom simptip-movable" data-tooltip="Mail" href="mailto:tommy@angklung.co" target="_blank"> </a></li>
									<li><a class="linkedin simptip-position-bottom simptip-movable" data-tooltip="LinkedIn" href="https://id.linkedin.com/in/tommykurniawans" target="_blank"> </a></li>
									<div class="clear"> </div>
								</li>
							</ul>
						</div>
						
						<div class="clear"> </div>
						<div class="team-member team-member-save">
							<span><a href="#team"><img src="<?= base_url()?>images/alvin-resmana.jpg"></a></span>
							<h4><a href="#team">Alvin Resmana</a></h4>
						   <label>Co-Founder</label>
							<p>Years of experience as iOS Developer.</p>
							<ul>
								<li>
									<li><a class="facebook simptip-position-bottom simptip-movable" data-tooltip="Facebook" href="https://www.facebook.com/vienz1st" target="_blank"> </a></li>
									<li><a class="mail simptip-position-bottom simptip-movable" data-tooltip="Mail" href="mailto:alvin@angklung.co" target="_blank"> </a></li>
									<li><a class="linkedin simptip-position-bottom simptip-movable" data-tooltip="LinkedIn" href="https://id.linkedin.com/pub/alvin-resmana/30/4b1/715" target="_blank"> </a></li>
									<div class="clear"> </div>
								</li>
							</ul>
						</div>
						<div class="team-member">
							<span><a href="#team"><img src="<?= base_url()?>images/mega-tjoa.jpg"></a></span>
							<h4><a href="#team">Mega Tjoa</a></h4>
						   <label>Designer</label>
							<p>Years of experience in web, logo and any kind of design.</p>
							<ul>
								<li>
									<li><a class="facebook simptip-position-bottom simptip-movable" data-tooltip="Facebook" href="https://www.facebook.com/megawaty.tjoa" target="_blank"> </a></li>
									<li><a class="mail simptip-position-bottom simptip-movable" data-tooltip="Mail" href="mailto:mega@angklung.co" target="_blank"> </a></li>
									<li><a class="linkedin simptip-position-bottom simptip-movable" data-tooltip="LinkedIn" href="https://id.linkedin.com/pub/megawaty-tjoa/69/75/991" target="_blank"> </a></li>
									<li><a class="behance simptip-position-bottom simptip-movable" data-tooltip="Behance" href="https://www.behance.net/tjoamegawaty" target="_blank"> </a></li>
									<div class="clear"> </div>
								</li>
							</ul>
						</div>
						<!-- <div class="team-member">
							<span><a href="#team"><img src="<?= base_url()?>images/satrio-adi.jpg"></a></span>
							<h4><a href="#team">Satrio Adi</a></h4>
						   <label>Designer</label>
							<p>Years of experience in web, logo and any kind of design.</p>
							<ul>
								<li>
									<li><a class="facebook simptip-position-bottom simptip-movable" data-tooltip="Facebook" href="https://www.facebook.com/crearthink" target="_blank"> </a></li>
									<li><a class="mail simptip-position-bottom simptip-movable" data-tooltip="Mail" href="mailto:satrio@angklung.co" target="_blank"> </a></li>
									<li><a class="linkedin simptip-position-bottom simptip-movable" data-tooltip="LinkedIn" href="https://id.linkedin.com/pub/satrio-adi-nugroho/65/439/a42" target="_blank"> </a></li>
									<li><a class="pin simptip-position-bottom simptip-movable" data-tooltip="Pintrest" href="https://www.pinterest.com/satrioadi/" target="_blank"> </a></li>
									<div class="clear"> </div>
								</li>
							</ul>
						</div>
						<div class="clear"> </div>
						<div class="clear"> </div>
					</div>
				</div>
			</div>
			-->
			<!---//End-meet-our-team---->
			
			<!---start-contact---->
			<div class="contact" id="contact">
				<div class="wrap">
				<div class="contact-head">
					<h3>Contact us</h3>
					<p>Have something in your head that need to pop out, let's talk with us and together we make it happen, contact us know.<br/>
					Or you send directly to our mail <a href="mailto:founders@angklung.co">founders@angklung.co</a></p>	
				</div>
				<div class="contatct-form">
					<form action="<?=base_url()?>root/email" method="post">
						<input name="name" required type="text" value="*Name :" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = '*Name :';}">
						<input name="email" required type="email" value="*Email :" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = '*Email :';}">
						<textarea name="message" required rows="2" cols="70" onfocus="if(this.value == '*Message :') this.value='';" onblur="if(this.value == '') this.value='*Message :';" >*Message :</textarea>
						<input type="submit" value="Send" />
					</form>
				</div>
			</div>
			</div>
			<!---End-contact---->
		</div>
		<!---End-content---->
		<!----start-footer---->
		<div class="footer">
			<div class="wrap">
				<div class="footer-grids">
					<div class="footer-left">
						<ul>
							<li><a class="fface" href="https://www.facebook.com/Angklungstudio"> </a></li>
							<li><a class="fhtml" href="#"> </a></li>
							<div class="clear"> </div>
						</ul>
					</div>
					<div class="footer-right">
						<p>Copyright by <a href="http://angklung.co/">Angklung Studio</a> @ 2015</p>
									<script type="text/javascript">
						$(document).ready(function() {
							$().UItoTop({ easingType: 'easeOutQuart' });
						});
					</script>
					<!----move-top-path---->
					<script type="text/javascript" src="<?= base_url()?>js/move-top.js"></script>
					<script type="text/javascript" src="<?= base_url()?>js/easing.js"></script>
					<script type="text/javascript">
						jQuery(document).ready(function($) {
							$(".scroll").click(function(event){		
								event.preventDefault();
								$('html,body').animate({scrollTop:$(this.hash).offset().top},1200);
							});
						});
					</script>
					<!----move-top-path---->
			    <a href="#" id="toTop" style="display: block;"><span id="toTopHover" style="opacity: 1;"></span></a>

					</div>
					<div class="clear"> </div>
				</div>
			</div>
		</div>
		<!----//End-footer---->
		<!---End-wrap--- -->
	</body>
</html>

