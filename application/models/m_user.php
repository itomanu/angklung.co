<?php
class M_User extends CI_Model {

    function insert()
    {
        $data = array(
            'name' => $this->input->post('name'),
            'email' => $this->input->post('email'),
            'message' => $this->input->post('message')
        );

        $this->load->database();
        $this->db->insert('users', $data);
    }
    
}

