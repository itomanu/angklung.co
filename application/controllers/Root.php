<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Root extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{	
		$this->load->view('welcome_message');
	}

	public function email()
	{
		if (!empty($_POST['name']) && !empty($_POST['email']) && !empty($_POST['message'])) {

			$data = array(
	            'name' => $this->input->post('name'),
	            'email' => $this->input->post('email'),
	            'message' => $this->input->post('message')
	        );

			$this->load->database();
        	
        	if ($this->db->insert('users', $data)) {
				$this->_sendEmail($email);
			} else {
				echo "Bad news";
			}
		} else {
			echo "Bad news";
			redirect('/');
		}
	}

	private function _sendEmail($email)
	{
		$data = Array(
		    'name' => 'Victor Manu',
		    'email' => $email
      	);

		$config = Array(
			'protocol' => 'smtp',
			'smtp_host' => 'mail.angklung.co',
			'smtp_port' => 26,
			'smtp_user' => 'founders@angklung.co',
			'smtp_pass' => 'WgpV#fyUiQVs', 
			'mailtype' => 'html',
			'charset' => 'iso-8859-1',
			'wordwrap' => TRUE
		);

		$this->load->library('email', $config);
		$this->email->set_newline("\r\n");
		$this->email->from('founders@angklung.co', 'Angklung Studio');
		$this->email->to($email);
		$this->email->subject("Thanks from Angklung Studio");
		$this->email->message($this->load->view('thanks_mail', $data, TRUE));

		if($this->email->send()) {
			redirect('/');
		} else {
			redirect('/');
			// show_error($this->email->print_debugger());
		}
	}
}
